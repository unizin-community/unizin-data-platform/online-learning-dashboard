CREATE TABLE IF NOT EXISTS dataset.12_lms_events_by_day (
  /* Event day */
  event_day DATE,

  /* Number of events */
  num_events INT64,
  num_tool_launches INT64,
  num_learner_activities_submitted INT64,
  num_quizzes_submitted INT64,
  num_things_created INT64,
  num_things_graded INT64
)
PARTITION BY event_day
