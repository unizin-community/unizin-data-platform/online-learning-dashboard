CREATE TABLE IF NOT EXISTS dataset.4_ip_locations (
  client_ip STRING,
  longitude FLOAT64,
  latitude FLOAT64,
  location_point GEOGRAPHY,
  city_name STRING,
  subdivision_1_iso_code STRING,
  subdivision_1_name STRING,
  postal_code STRING,
  country_iso_code STRING,
  country_name STRING,
  continent_code STRING,
  continent_name STRING,
  time_zone STRING
)
