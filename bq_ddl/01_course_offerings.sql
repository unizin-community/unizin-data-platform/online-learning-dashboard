CREATE TABLE IF NOT EXISTS dataset.<store>_course_offerings (
  academic_term_ucdm_id INT64,
  academic_term_name STRING,
  course_offering_ucdm_id INT64,
  course_offering_sis_id STRING,
  course_offering_lms_id STRING,
  course_unique_name STRING,
  course_offering_status STRING,
  is_course_offering_published BOOLEAN,
  course_subject STRING,
  course_number STRING,
  course_code STRING,
  course_title STRING,
  course_student_enrollments INT64,
  course_instructor_enrollments INT64
)
