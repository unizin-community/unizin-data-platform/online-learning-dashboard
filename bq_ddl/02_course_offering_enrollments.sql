CREATE TABLE IF NOT EXISTS dataset.<store>_course_offering_enrollments (
  course_offering_ucdm_id INTEGER,
  course_offering_lms_id STRING,
  actor_ucdm_id INTEGER,
  actor_lms_id STRING,
  actor_full_name STRING,
  role STRING,
  role_status STRING
)
