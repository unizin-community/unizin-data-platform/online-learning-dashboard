#!/usr/bin/env perl

#### Exports all of the required UDW-source data
#### for the UDP Dashboard project.

use Data::Dumper;
use DBI;
use open ':std', ':encoding(UTF-8)';
use strict;
use Text::CSV;
use utf8;
use warnings;
use YAML;
use YAML::XS 'LoadFile';

my $home = '/path/to/home';
my $canvas_id = ; # Set to Canvas ID prefix

my $f_conf = $home . '/config.yaml';
my $config = LoadFile($f_conf);

my $cs  = &connect_to_database(
  $config->{cs}->{name},
  $config->{cs}->{host},
  $config->{cs}->{port},
  $config->{cs}->{user},
  $config->{cs}->{pass}
);

my $udw  = &connect_to_database(
  $config->{udw}->{name},
  $config->{udw}->{host},
  $config->{udw}->{port},
  $config->{udw}->{user},
  $config->{udw}->{pass}
);

###################################################
#### Print all of the data about this shizzle. ####
my $csv =
  Text::CSV->new({ binary => 1, sep_char => ',' })
    or die "Cannot use CSV: ".Text::CSV->error_diag ();

##################################
#### Run queries, export data ####
&run_export(
  $cs,
  'cs',
  'course_offering_enrollments',
  [
    'course_offering_ucdm_id',
    'course_offering_lms_id',
    'actor_ucdm_id',
    'actor_lms_id',
    'actor_full_name',
    'role',
    'role_status'
  ]
);

&run_export(
  $cs,
  'cs',
  'course_offerings',
  [
    'academic_term_ucdm_id',
    'academic_term_name',
    'course_offering_ucdm_id',
    'course_offering_sis_id',
    'course_offering_lms_id',
    'course_unique_name',
    'course_offering_status',
    'is_course_offering_published',
    'course_subject',
    'course_number',
    'course_code',
    'course_title',
    'course_student_enrollments',
    'course_instructor_enrollments'
  ]
);

&run_export(
  $udw,
  'udw',
  'course_offering_enrollments',
  [
    'course_offering_ucdm_id',
    'course_offering_lms_id',
    'actor_ucdm_id',
    'actor_lms_id',
    'actor_full_name',
    'role',
    'role_status'
  ]
);

&run_export(
  $udw,
  'udw',
  'course_offerings',
  [
    'academic_term_ucdm_id',
    'academic_term_name',
    'course_offering_ucdm_id',
    'course_offering_sis_id',
    'course_offering_lms_id',
    'course_unique_name',
    'course_offering_status',
    'is_course_offering_published',
    'course_subject',
    'course_number',
    'course_code',
    'course_title',
    'course_student_enrollments',
    'course_instructor_enrollments'
  ]
);

#######################################
#### Disconnect from the database. ####
&disconnect_from_database($cs);
&disconnect_from_database($udw);

exit;

################################################################################
sub run_export($$$) {
  my($db, $store, $concept, $params) = @_;

  print "Running $concept on $store \n";

  ##############################################
  #### Ingest the query we're going to run. ####
  my $query_file = "$home/export_course_offering_data/${store}_$concept.sql";

  open
    my $fh,
    "<:encoding(utf8)",
    $query_file
      or die "$query_file: $!";

  my $query = '';
  while( my $line = <$fh> ) {
    $query .= " $line";
  }

  close($fh);

  #######################
  #### Run the query ####
  my $output_file = "$home/gcs-data/${store}_$concept.csv";

  open
    $fh,
    ">:encoding(utf8)",
    $output_file
      or die "$output_file: $!";

  my $sth = $db->prepare($query);

  if( $concept eq 'course_offering_enrollments') {
    $sth->execute($canvas_id);
  } else {
    $sth->execute;
  }

  while(my $row = $sth->fetchrow_hashref) {
    my @data = ();

    foreach my $param (@$params) {
      push( @data, $row->{$param} );
    }

    $csv->print($fh, \@data);
    print $fh "\n";
  }

  $sth->finish;
  close($fh);
}

################################################################################
#################### Database functions ########################################
sub connect_to_database {
  my( $cs_name, $cs_host, $cs_port, $cs_user, $cs_pass ) = @_;

  my $dbh = DBI->connect("dbi:Pg:dbname=$cs_name;host=$cs_host;port=$cs_port", $cs_user, $cs_pass)
    || die "Could not connect to database\n";

  $dbh->{AutoCommit} = 0;
  $dbh->{RaiseError} = 1;
  $dbh->do("SET TIME ZONE 'UTC'");

  return $dbh;
}

sub disconnect_from_database {
  my($dbh) = @_;

  $dbh->disconnect;

  return 1;
}
