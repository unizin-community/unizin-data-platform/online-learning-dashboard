WITH teaching_assignments AS (
  SELECT
    co.course_offering_id AS course_offering_id
    , count(cse.person_id) AS num_instructors

  FROM
    entity.course_section_enrollment cse
  INNER JOIN entity.person p ON cse.person_id=p.person_id
  INNER JOIN entity.course_section cs ON cse.section_id=cs.course_section_id
  INNER JOIN entity.course_offering co USING(course_offering_id)
  INNER JOIN keymap.course_offering kco ON co.course_offering_id=kco.id

  WHERE
    role = 'Teacher'
    AND
      (
        role_status != 'Withdrawn'
        OR role_status != 'Dropped'
      )

  GROUP BY
    co.course_offering_id
),

student_enrollments AS (
  SELECT
    co.course_offering_id AS course_offering_id
    , count(cse.person_id) AS num_students

  FROM
    entity.course_section_enrollment cse
  INNER JOIN entity.course_section cs ON cse.section_id=cs.course_section_id
  INNER JOIN entity.course_offering co USING(course_offering_id)
  INNER JOIN keymap.course_offering kco ON co.course_offering_id=kco.id

  WHERE
    role = 'Student'
    AND
      (
        role_status != 'Withdrawn'
        OR role_status != 'Dropped'
      )

  GROUP BY
    co.course_offering_id
)

SELECT
  at.academic_term_id AS academic_term_ucdm_id
  , at.name AS academic_term_name
  , co.course_offering_id AS course_offering_ucdm_id
  , kco.sis_int_id AS course_offering_sis_id
  , CASE
    WHEN kco.lms_int_id IS NOT NULL THEN kco.lms_int_id
    ELSE 'unknown_lms_id'
    END AS course_offering_lms_id
  , CASE
    WHEN kco.lms_int_id IS NOT NULL THEN concat( co.title, ' (', kco.lms_int_id, ')')
    ELSE concat( co.title, ' (No LMS Course)')
    END AS course_unique_name
  , CASE
    WHEN co.status = 'created' THEN 'Created' -- created, not published
    WHEN co.status = 'claimed' THEN 'Claimed' -- undeleted, not published
    WHEN co.status = 'deleted' THEN 'Deleted' -- deleted, not published
    WHEN co.status = 'available' THEN 'Available' -- published
    WHEN co.status = 'completed' THEN 'Completed' -- published
    WHEN co.status = 'Active' THEN 'Active' -- SIS Active, not published
    WHEN co.status = 'Inactive' THEN 'Inactive' -- SIS Inactive, not published
    END AS course_offering_status
  , CASE
    WHEN co.status = 'Avaialble' OR co.status = 'Completed' THEN TRUE
    ELSE FALSE
    END AS is_course_offering_published
  , co.course_subj AS course_subject
  , co.course_no AS course_number
  , concat( co.course_subj, ' ', co.course_no ) AS course_code
  , co.title AS course_title
  , CASE
    WHEN se.num_students IS NOT NULL AND se.num_students > 0 THEN se.num_students
    ELSE 0
    END AS course_student_enrollments
  , CASE
    WHEN ta.num_instructors IS NOT NULL AND ta.num_instructors > 0 THEN ta.num_instructors
    ELSE 0
    END AS course_instructor_enrollments

FROM
  entity.course_offering co
INNER JOIN entity.academic_term at USING (academic_term_id)
INNER JOIN keymap.course_offering kco ON co.course_offering_id=kco.id
LEFT JOIN student_enrollments se USING (course_offering_id)
LEFT JOIN teaching_assignments ta USING (course_offering_id)
