-- Create table: 06_all_events_by_day

WITH dates AS (
  SELECT
    event_day
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2020-01-01', CURRENT_DATE(), INTERVAL 1 DAY ) ) AS event_day
  -- TO DO: Write a version of this that works with a single day.
  --
),

all_events_by_day AS (
  SELECT

    /* Date */
    r.event_day event_day

    /* Total number of events for the person */
    , COUNT(r.event_day) num_events

    /* Summarizing things that were done. */
    , SUM(IF(r.web_resource_location_type = 'context_external_tool', 1, 0)) num_tool_launches
    , SUM(IF(r.event_action  = 'Submitted', 1, 0)) num_learner_activities_submitted
    , SUM(IF(r.event_action  = 'Submitted' AND r.assignable_type = 'Assessment', 1, 0)) num_quizzes_submitted
    , SUM(IF(r.event_action  = 'Created', 1, 0)) num_things_created
    , SUM(IF(r.event_action  = 'Graded', 1, 0)) num_things_graded

  FROM
    dataset.05_enriched_events r

  WHERE
    event_day >= '2020-01-01'

  GROUP BY
    r.event_day
)

SELECT
  /* Date */
  d.event_day AS event_day

  /* Total number of events for the person */
  , COALESCE(aebd.num_events, 0) num_events

  /* Summarizing things that were done. */
  , COALESCE(aebd.num_tool_launches, 0) num_tool_launches
  , COALESCE(aebd.num_learner_activities_submitted, 0) num_learner_activities_submitted
  , COALESCE(aebd.num_quizzes_submitted, 0) num_quizzes_submitted
  , COALESCE(aebd.num_things_created, 0) num_things_created
  , COALESCE(aebd.num_things_graded, 0) num_things_graded

FROM
  dates d

LEFT JOIN all_events_by_day aebd ON d.event_day = aebd.event_day
