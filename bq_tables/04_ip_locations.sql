/*
  Target table: 04_ip_locations
*/

WITH ip_addresses AS (
  SELECT
    DISTINCT(r.client_ip) client_ip
  FROM
    dataset.3_expanded_events r
  WHERE
    r.client_ip IS NOT NULL
)

SELECT
  /* IP Data */
  ip_network_binary.client_ip,

  /* Coordinates */
  locations.longitude,
  locations.latitude,
  ST_GeogPoint(AVG(locations.longitude), AVG(locations.latitude)) location_point,

  /* Location meadata */
  locations.city_name,
  locations.subdivision_1_iso_code,
  locations.subdivision_1_name,
  locations.postal_code,
  locations.country_iso_code,
  locations.country_name,
  locations.continent_code,
  locations.continent_name,
  locations.time_zone

FROM (
  SELECT
    NET.SAFE_IP_FROM_STRING(ia.client_ip) & NET.IP_NET_MASK(4,mask) network_bin
    , ia.client_ip client_ip
    , mask mask
  FROM
    ip_addresses ia,
    UNNEST(GENERATE_ARRAY(9,32)) mask
  WHERE
    BYTE_LENGTH(NET.SAFE_IP_FROM_STRING(ia.client_ip)) = 4
  ) as ip_network_binary

JOIN
  `fh-bigquery.geocode.201806_geolite2_city_ipv4_locs` locations

USING
  (network_bin, mask)

GROUP BY
  ip_network_binary.client_ip,
  locations.longitude,
  locations.latitude,
  locations.city_name,
  locations.subdivision_1_iso_code,
  locations.subdivision_1_name,
  locations.postal_code,
  locations.country_iso_code,
  locations.country_name,
  locations.continent_code,
  locations.continent_name,
  locations.time_zone
