/*
  Target table: 03_expanded_events
*/

SELECT

  /****************/
  /*  Date & time */
  /****************/
  event_time event_time
  , EXTRACT(DATE FROM DATETIME(event_time)) AS event_day
  , EXTRACT(HOUR FROM DATETIME(event_time)) AS event_hour

  /************/
  /*  Ed App  */
  /************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN "https://canvas.instructure.com"
    WHEN  ed_app LIKE "%kaltura%" THEN "https://kaltura.com"
    ELSE  ed_app
  END ed_app

  /********************/
  /*  Type and Action */
  /********************/
  , type event_type
  , action event_action

  /****************/
  /*  Actor data  */
  /****************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"(\d+)$")
    WHEN  ed_app LIKE "%kaltura%"                                THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"user/(\w+)$")
    WHEN  ed_app = "https://inscribe.education"                  THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"users/(\d+)$")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"user:(\d+)$")
    WHEN  ed_app = "https://app.tophat.com"                      THEN JSON_EXTRACT_SCALAR(event, "$[actor][id]")
    WHEN  ed_app = "https://quickcheck.eds.iu.edu"               THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"https://quickcheck.eds.iu.edu/api/caliper/student/(\d+)$")
    ELSE  NULL
  END actor_id

  , ucdm_actor_id actor_ucdm_id

  , JSON_EXTRACT_SCALAR(event, "$[actor][extensions]['com.instructure.canvas'][user_sis_id]") AS actor_sis_id -- Canvas only

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[actor][id]"), r"(\d+)$")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN JSON_EXTRACT_SCALAR(event, "$[federatedSession][messageParameters][custom_canvas_user_id]")
    WHEN  ed_app = "https://inscribe.education"                  THEN JSON_EXTRACT_SCALAR(event, "$[federatedSession][messageParameters][custom_canvas_user_id]")
    ELSE NULL
    END actor_lms_id

  , JSON_EXTRACT_SCALAR(event, "$[actor][extensions]['com.instructure.canvas'][user_login]") AS actor_login -- Canvas only
  , JSON_EXTRACT_SCALAR(event, "$[actor][type]") AS actor_type
  , JSON_EXTRACT_SCALAR(event, "$[actor][name]") AS actor_name -- Inscribe only

  /*****************/
  /*  Course data  */
  /*****************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[membership][organization][id]"), r"(\d+)$")
    WHEN  ed_app LIKE "%kaltura%"                                THEN JSON_EXTRACT_SCALAR(event, "$[object][extensions]['kaf:course_id']")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[group][subOrganizationOf][id]"), r"course:(\d+)$")
    WHEN  ed_app = "https://app.tophat.com"                      THEN JSON_EXTRACT_SCALAR(event, "$[membership][organization]")
    ELSE  NULL
  END course_offering_id

  , ucdm_course_offering_id course_offering_ucdm_id
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[membership][organization][id]"), r"(\d+)$")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN JSON_EXTRACT_SCALAR(event, "$[federatedSession][messageParameters][custom_canvas_course_id]")
    WHEN  ed_app = "https://inscribe.education"                  THEN JSON_EXTRACT_SCALAR(event, "$[federatedSession][messageParameters][custom_canvas_course_id]")
    ELSE NULL
    END course_offering_lms_id

  /*******************/
  /* Course section? */
  /*******************/

  /***************/
  /*  Role data  */
  /***************/
  , JSON_EXTRACT_SCALAR(event, "$[membership][roles][0]") AS actor_role -- Canvas and Tophat

  /*****************/
  /*  Object data  */
  /*****************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][id]"), r"(\d+)$")
    WHEN  ed_app LIKE "%kaltura%"                                THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][id]"), r"media/(\w+)$")
    WHEN  ed_app = "https://inscribe.education"                  THEN JSON_EXTRACT_SCALAR(event, "$[object][id]")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][id]"), r"page:(\d+)$")
    WHEN  ed_app = "https://app.tophat.com"                      THEN JSON_EXTRACT_SCALAR(event, "$[object][id]")
    WHEN  ed_app = "https://quickcheck.eds.iu.edu"               THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][id]"), r"https://quickcheck.eds.iu.edu/api/caliper/student/(\d+)$")
    ELSE  NULL
  END object_id

  , JSON_EXTRACT_SCALAR(event, "$[object][type]") AS object_type

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][id]"), r":(\w+):\d+$")
    ELSE  NULL
  END object_entity

  , JSON_EXTRACT_SCALAR(event, "$[object][name]") AS object_name
  , CAST( JSON_EXTRACT_SCALAR(event, "$[object][dateToSubmit]") AS TIMESTAMP) AS object_due_date -- In Canvas; how many distinct of these are there?
  , JSON_EXTRACT_SCALAR(event, "$[object][maxScore][numberStr]") AS object_max_score -- In Canvas; how many distinct of these are there?
  , JSON_EXTRACT_SCALAR(event, "$[object][duration]") AS object_duration -- In Kaltura
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][entity_id]") AS object_entity_id -- Canvas only
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][filename]") AS object_filename -- Canvas only

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN JSON_EXTRACT_SCALAR(event, "$[object][mediaType]")
    ELSE NULL
  END object_mime_type

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][submission_type]")
    ELSE NULL
    END object_entity_type

  , JSON_EXTRACT_SCALAR(event, "$[object][extensions][topicNames]") AS object_topic_names -- Inscribe only
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions][topicSlugs]") AS object_topic_slugs -- Inscribe only

  -- Is part of
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][isPartOf][id]"), r"(\d+)$")
    WHEN  ed_app = "https://engage.unizin.org"                   THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][isPartOf][id]"), r"(\d+)$")
    ELSE NULL
  END is_part_of_id

  , JSON_EXTRACT_SCALAR(event, "$[object][isPartOf][type]") AS is_part_of_type -- Canvas only

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][isPartOf][id]"), r":(\w+):\d+$")
    ELSE NULL
  END is_part_of_entity

  , JSON_EXTRACT_SCALAR(event, "$[object][isPartOf][name]") AS is_part_of_name -- Engage only

  /* TO-DO: Engage's document and page as separate objects? */

  -- Assignee data (always a person)
  , JSON_EXTRACT_SCALAR(event, "$[object][assignee][id]") AS assignee_id -- In Canvas, user_id, needs parsing
  , JSON_EXTRACT_SCALAR(event, "$[object][assignee][type]") AS assignee_type -- In Canvas

  -- Assignable data
  , JSON_EXTRACT_SCALAR(event, "$[object][assignable][id]") AS assignable_id -- In Canvas, quiz_id (perhaps more); needs parsing; how many patterns of this are there?
  , JSON_EXTRACT_SCALAR(event, "$[object][assignable][type]") AS assignable_type -- In Canvas; how many distinct of these are there?

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[object][assignable][id]"), r":(\w+):\d+$")
    ELSE NULL
  END assignable_entity


  /**********************/
  /** Generated object **/
  /**********************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[generated][id]"), r"(\d+)$")
    WHEN  ed_app = "https://quickcheck.eds.iu.edu"               THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[generated][id]"), r"https://quickcheck.eds.iu.edu/api/caliper/attempt/(\d+)$")
    ELSE NULL
  END generated_id

  , JSON_EXTRACT_SCALAR(event, "$[generated][type]") AS generated_type

  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[generated][id]"), r":(\w+):\d+$")
    ELSE NULL
  END generated_entity

  -- Canvas-only values.
  , JSON_EXTRACT_SCALAR(event, "$[generated][scoreGiven][numberStr]") AS generated_grade
  , JSON_EXTRACT_SCALAR(event, "$[generated][maxScore][numberStr]") AS generated_max_grade
  , JSON_EXTRACT_SCALAR(event, "$[generated][scoredBy]") AS generated_scored_by

  /*******************/
  /** Target object **/
  /*******************/
  , CASE
    WHEN  ed_app LIKE "%kaltura%"               THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[target][id]"), r"caliper/info/media/(.+)$")
    WHEN  ed_app = "https://inscribe.education" THEN REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(event, "$[target][id]"), r"communities/(\w+)$")
    ELSE NULL
  END target_id

  , JSON_EXTRACT_SCALAR(event, "$[target][type]") AS target_type
  , JSON_EXTRACT_SCALAR(event, "$[target][extensions]['kaf:watch_percentage']") AS target_watch_percentage
  , JSON_EXTRACT_SCALAR(event, "$[target][currentTime]") AS target_current_time

  /*******************/
  /** Site location **/
  /*******************/
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_type]") AS web_resource_location_type
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_subtype]") AS web_resource_location_subtype
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_name]") AS web_resource_location_name

  /****************/
  /** App launch **/
  /****************/
  , CASE
    WHEN (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%") AND JSON_EXTRACT_SCALAR(event, "$[object][name]") = 'context_external_tool' THEN TRUE
    ELSE FALSE
  END is_tool_launch
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][url]") AS launch_app_url
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][domain]") AS launch_app_domain
  , CASE
    WHEN
      (ed_app LIKE "%canvas%" OR ed_app LIKE "%instructure%")
      AND 'context_external_tool' = JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_type]")
      THEN JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_name]")
    ELSE NULL
    END launch_app_name
  , CASE
    WHEN JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][entity_id]") IS NOT NULL
         AND JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][domain]") != 'edu-apps.org'
         THEN TRUE
         ELSE FALSE
    END is_tool

  /****************/
  /** IP Address **/
  /****************/
  , JSON_EXTRACT_SCALAR(event, "$[extensions]['com.instructure.canvas'][client_ip]") AS client_ip

FROM
  udp-<tenant>-prod.event_store.events
WHERE
  event_time >= '2020-01-01 00:00:00'
;
