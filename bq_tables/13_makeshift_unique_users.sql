Select
  event_day
  , count(distinct actor_lms_id) unique_users
FROM
  dataset.05_enriched_events
WHERE
  actor_lms_id IS NOT NULL
  AND course_offering_lms_id IS NOT NULL
  AND ed_app = 'https://canvas.instructure.com'
GROUP BY
  event_day
ORDER BY
  event_day ASC
