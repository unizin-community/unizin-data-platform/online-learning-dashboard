-- Create table: 12_lms_events_by_day

/*
  Reasons why there may be fewer events summed in this
  table than in the 5_enriched_events table:

  * Not every event in 5_enriched_events is an LMS event (and therefore
    has a course_offering_lms_id).

  * Not every LMS event takes place in a course and therefore has a
    course_offering_lms_id.

  * Not every LMS event is performed by an individual (and therefore
    has an actor_lms_id).

*/

WITH dates AS (
  SELECT
    event_day
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2020-01-01', CURRENT_DATE(), INTERVAL 1 DAY ) ) AS event_day
  -- TO DO: Write a version of this that works with a single day.
  --
),

by_day AS (
  SELECT

    /* Date */
    r.event_day event_day

    /* Total number of events for the person */
    , COUNT(r.event_day) num_events

    /* Summarizing things that were done. */
    , SUM(IF(r.web_resource_location_type = 'context_external_tool', 1, 0)) num_tool_launches
    , SUM(IF(r.event_action  = 'Submitted', 1, 0)) num_learner_activities_submitted
    , SUM(IF(r.event_action  = 'Submitted' AND r.assignable_type = 'Assessment', 1, 0)) num_quizzes_submitted
    , SUM(IF(r.event_action  = 'Created', 1, 0)) num_things_created
    , SUM(IF(r.event_action  = 'Graded', 1, 0)) num_things_graded

  FROM
    dataset.05_enriched_events r

  WHERE
    ed_app = 'https://canvas.instructure.com'
    AND event_day >= '2020-01-01'

  GROUP BY
    r.event_day
)

SELECT
  /* Date */
  d.event_day AS event_day

  /* Total number of events for the person */
  , COALESCE(bd.num_events, 0) num_events

  /* Summarizing things that were done. */
  , COALESCE(bd.num_tool_launches, 0) num_tool_launches
  , COALESCE(bd.num_learner_activities_submitted, 0) num_learner_activities_submitted
  , COALESCE(bd.num_quizzes_submitted, 0) num_quizzes_submitted
  , COALESCE(bd.num_things_created, 0) num_things_created
  , COALESCE(bd.num_things_graded, 0) num_things_graded

FROM
  dates d

LEFT JOIN by_day bd
ON
  (
    d.event_day = bd.event_day
  )
;
